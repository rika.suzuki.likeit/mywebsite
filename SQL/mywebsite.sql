create database mywebsite default character set utf8;
use mywebsite;

insert into user_data values(1,'suzuki','管理者','日本','1111');

create table item(
	id serial,
	name varchar(255) unique not null,
	detail text not null,
	price int not null,
	image varchar(255) not null,
	category int not null
);

create table buy(
	id serial,
	user_id int,
	total_price int,
	buy_date datetime,
	delivery int
);


create table buy_history(
	id serial,
	buy_id int,
	item_id int,
	item_size int
);

create table delivery(
	id serial,
	name varchar(255),
	price int
);

create table category(
	id serial,
	name varchar(255)
);

create table item_size(
	id serial,
	name varchar(255)
);

insert into item values
(1,'ゴムベルト付きウェーブスカート',
'細かいウェーブ仕立てがポイントになったスカートは、ゴムベルト付きの優れもの♪
ゆったりとしたシルエット&ウエスト部分はゴム仕様なのでラフに着用して頂けます◎
大きめのパーカーなどと合わせると、今年らしい旬なスタイリングが出来上がり！

[ベルト]長さ約67.5cm/ベルト幅約1cm/バックル縦約2cm/バックル横約6.5cm

＊摩擦や水濡れによる色落ち・色移りにご注意下さい。
＊デリケートな素材を使用しておりますのでお取扱いにご注意下さい。
＊この製品はプリーツ加工を施しております。
＊加工は永久的なものではありませんので、お取扱いにご注意下さい。',
5500,'image1.jpg',2),
(2,'チェックボウタイワンピース',
'レトロな雰囲気たっぷりのチェック柄がpointになったワンピースが登場しました♪
首元のボウタイデザインが、より女の子らしいcuteな印象を与えてくれるのもポイント！
1枚で着るのはもちろん、カーディガンなどを羽織ったコーディネートもおすすめです◎',
6380,'image2.jpg',4),
(3,'ウエストキュッとアウター',
'ウエストリボンをキュッと結ぶだけで、スタイルUP効果に繋がるオススメのアウター！
大きめのフードが、顔周りをすっきりとみせてくれるのも嬉しいポイントになります◎
1枚で愛らしいスタイリングを叶えてくれるアイテムは、持っていると女子力高まる♪

＊摩擦や水濡れによる色落ち・色移り・色泣きにご注意下さい。
＊素材の特性上、毛羽立ち・毛乱れ・毛抜け・ピリング(毛玉)が生じますのでご注意下さい。',
12650,'image3.jpg',3);


insert into delivery values
(1,'通常配送',600),
(2,'速達',1000);

insert into category values
(1,'トップス'),
(2,'ボトムス'),
(3,'アウター'),
(4,'ワンピース'),
(5,'シューズ'),
(6,'バッグ'),
(7,'アクセサリー'),
(8,'その他');

insert into item_size values
(1,'S'),
(2,'M'),
(3,'L');

select * from buy join delivery on buy.delivery = delivery.id where buy.user_id = 1;

select * from item order by id;

update user_data set login_id ='okada', name = 'みそ', address = '東京' where id = 3;

select * from user_data where id = 1;
