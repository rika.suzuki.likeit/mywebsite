package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.ItemBeans;

public class ItemDao {

	//ピックアップ商品をランダムに選択
	public static ArrayList<ItemBeans> pickupItem(int itemNum){
		Connection con = null;
		ArrayList<ItemBeans> puItemList = new ArrayList<ItemBeans>();
		try {
			con = DBManager.getConnection();
			String sql = "select * from item order by rand() limit ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, itemNum);
			ResultSet rs = stmt.executeQuery();


			while(rs.next()) {
				ItemBeans randItem = new ItemBeans();
				randItem.setId(rs.getInt("id"));
				randItem.setName(rs.getString("name"));
				randItem.setPrice(rs.getInt("price"));
				randItem.setImage(rs.getString("image"));
				puItemList.add(randItem);
			}

		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return puItemList;
	}

	//登録された全商品の情報を取得
	public static ArrayList<ItemBeans> AllItem() {
		Connection con = null;
		ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();

		try {
			con = DBManager.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from item");
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				ItemBeans item = new ItemBeans();
				item.setId(Integer.parseInt(rs.getString("id")));
				item.setName(rs.getString("name"));
				item.setPrice(Integer.parseInt(rs.getString("price")));
				itemList.add(item);
			}

		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return itemList;
	}

	//商品IDから商品詳細を取得
	public static ItemBeans itemDetail(String id) {
		Connection con = null;
		ItemBeans itemDetail = new ItemBeans();

		try {
			con = DBManager.getConnection();
			String sql = "select *"
					+ " from item"
					+ " join category"
					+ " on item.category = category.id"
					+ " where item.id = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, id);
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				itemDetail.setId(rs.getInt("id"));
				itemDetail.setName(rs.getString("name"));
				itemDetail.setDetail(rs.getString("detail"));
				itemDetail.setPrice(rs.getInt("price"));
				itemDetail.setImage(rs.getString("image"));
				itemDetail.setCategory(rs.getInt("category"));
				itemDetail.setCategoryName(rs.getString("category.name"));
			}

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return itemDetail;
	}

	//商品を新規登録
	public static int itemRegist(ItemBeans itemRegist) {
		Connection con = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			String sql = "insert into item (name, detail, price, image, category) value(?,?,?,?,?)";
			PreparedStatement stmt = con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, itemRegist.getName());
			stmt.setString(2, itemRegist.getDetail());
			stmt.setInt(3, itemRegist.getPrice());
			stmt.setString(4, itemRegist.getImage());
			stmt.setInt(5, itemRegist.getCategory());
			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return autoIncKey;
	}

	//商品を削除
	public static void itemDelete(String id) {
		Connection con = null;
		try {
			con = DBManager.getConnection();
			String sql = "delete from item where id = ? ";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, id);
			stmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//商品詳細の更新
	public static void itemUpdate(ItemBeans itemUpdate) {
		Connection con = null;
		try {
			con = DBManager.getConnection();
			String sql="update item set name = ?, detail = ?, price = ?, category = ?, image = ? where id = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, itemUpdate.getName());
			stmt.setString(2, itemUpdate.getDetail());
			stmt.setInt(3, itemUpdate.getPrice());
			stmt.setInt(4, itemUpdate.getCategory());
			stmt.setString(5, itemUpdate.getImage());
			stmt.setInt(6, itemUpdate.getId());
			stmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}
	}

	//検索ワードによる検索結果を取得する
	public static ArrayList<ItemBeans> searchItemByWord(String searchWord){
		Connection con = null;
		ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();
		PreparedStatement stmt = null;
		try {
			con = DBManager.getConnection();

			if(searchWord.length() == 0) {
				//検索ワードが未入力の場合、全検索
				stmt = con.prepareStatement("select * from item order by id");

			}else {
				stmt = con.prepareStatement("select * from item where name like ? order by id");
				stmt.setString(1, "%"+ searchWord + "%");

			}

			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				ItemBeans searchItem = new ItemBeans();
				searchItem.setId(rs.getInt("id"));
				searchItem.setName(rs.getString("name"));
				searchItem.setPrice(rs.getInt("price"));
				searchItem.setImage(rs.getString("image"));
				itemList.add(searchItem);
			}

		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return itemList;
	}

	//カテゴリー毎の検索結果を取得
	public static ArrayList<ItemBeans> searchItemByCategory(int categoryId){
		Connection con = null;
		ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();
		try {
			con = DBManager.getConnection();
			String sql = "select * from item join category"
					+ " on item.category = category.id"
					+ " where item.category = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, categoryId);
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				ItemBeans searchItem = new ItemBeans();
				searchItem.setId(rs.getInt("id"));
				searchItem.setName(rs.getString("name"));
				searchItem.setPrice(rs.getInt("price"));
				searchItem.setImage(rs.getString("image"));
				itemList.add(searchItem);
			}

		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return itemList;
	}
}
