package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.BuyDetailBeans;

public class BuyDetailDao {
	//購入詳細情報を登録
	public static void insertBuyDetailData(BuyDetailBeans buyDetailData) {
		Connection con = null;
		try{
			con = DBManager.getConnection();
			String sql = "insert into buy_history (buy_id, item_id, item_size) value (?,?,?)";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, buyDetailData.getBuyId());
			stmt.setInt(2, buyDetailData.getItemId());
			stmt.setInt(3, buyDetailData.getItemSize());
			stmt.executeUpdate();

		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//購入IDから購入履歴詳細を取得
	public static ArrayList<BuyDetailBeans> getBuyDetailData(int buyId) {
		Connection con = null;
		ArrayList<BuyDetailBeans> buyDetailData = new ArrayList<BuyDetailBeans>();
		try {
			con = DBManager.getConnection();
			String sql = "select * from buy_history"
					+ " join item_size on buy_history.item_size = item_size.id"
					+ " join item on buy_history.item_id = item.id"
					+ " where buy_id = ?";
			PreparedStatement stem = con.prepareStatement(sql);
			stem.setInt(1, buyId);
			ResultSet rs = stem.executeQuery();

			while(rs.next()){
				BuyDetailBeans bdd = new BuyDetailBeans();
				bdd.setId(rs.getInt("id"));
				bdd.setBuyId(rs.getInt("buy_id"));
				bdd.setItemId(rs.getInt("item_id"));
				bdd.setItemName(rs.getString("item.name"));
				bdd.setItemPrice(rs.getInt("item.price"));
				bdd.setItemImage(rs.getString("item.image"));
				bdd.setItemSize(rs.getInt("item_size"));
				bdd.setItemSizeName(rs.getString("item_size.name"));
				buyDetailData.add(bdd);
				}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return buyDetailData;

	}
}
