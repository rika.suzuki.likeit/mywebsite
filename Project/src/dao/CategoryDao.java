package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.CategoryBeans;



public class CategoryDao {

	/*DBの商品カテゴリーを取得*/
	public static ArrayList<CategoryBeans> getCategory(){
		Connection con = null;

		try {
			con = DBManager.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from category");
			ResultSet rs = stmt.executeQuery();

			ArrayList<CategoryBeans> categoryList = new ArrayList<CategoryBeans>();

			while(rs.next()) {
				CategoryBeans category = new CategoryBeans();
				category.setId(rs.getInt("id"));
				category.setName(rs.getString("name"));
				categoryList.add(category);
			}

			return categoryList;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally{
			if (con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//カテゴリーIDからカテゴリー名を取得
	public static CategoryBeans categoryDetail(int id) {
		Connection con = null;
		CategoryBeans categoryDetail = new CategoryBeans();
		try {
			con = DBManager.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from category where id = ?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				categoryDetail.setId(rs.getInt("id"));
				categoryDetail.setName(rs.getString("name"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return categoryDetail;
	}
}
