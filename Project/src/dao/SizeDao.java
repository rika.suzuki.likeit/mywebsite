package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.SizeBeans;

public class SizeDao {
	//商品サイズを取得
	public static ArrayList<SizeBeans> sizeList() {
		Connection con = null;
		ArrayList<SizeBeans> sizeList = new ArrayList<SizeBeans>();
		try {
			con = DBManager.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from item_size");
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				SizeBeans sList = new SizeBeans();
				sList.setId(rs.getInt("id"));
				sList.setName(rs.getString("name"));
				sizeList.add(sList);
			}

		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return sizeList;
	}

	//サイズIDから詳細を取得
	public static SizeBeans sizeDetail(int id) {
		Connection con = null;
		SizeBeans sizeDetail = new SizeBeans();
		try {
			con = DBManager.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from item_size where id = ?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				sizeDetail.setId(rs.getInt("id"));
				sizeDetail.setName(rs.getString("name"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return sizeDetail;
	}
}
