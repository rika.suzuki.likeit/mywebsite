package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.UserDataBeans;
import etc.etc;


public class UserDataDao {

	//入力されたログインIDが使用済みか確認
	public static boolean loginIdCheck(String loginId) {
		boolean idCheck = false;
		Connection con = null;
		try {
			con = DBManager.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from user_data where login_id = ?");
			stmt.setString(1, loginId);
			ResultSet rs = stmt.executeQuery();

			//ログインIDが使用済の場合
			if(rs.next()) {
				idCheck = true;
			}

		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			if(con !=  null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return idCheck;
	}

	//DBへ新規ユーザー登録
	public static void userRegist(UserDataBeans userData){
		Connection con = null;
		try {
			con = DBManager.getConnection();
			String sql = "insert into user_data(login_id, name, address, password) value(?,?,?,?)";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, userData.getLoginId());
			stmt.setString(2, userData.getName());
			stmt.setString(3, userData.getAddress());
			stmt.setString(4, etc.pass(userData.getPassword()));
			stmt.executeUpdate();

		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try{
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ログイン可否
	public static UserDataBeans loginCheck(UserDataBeans userData) {
		Connection con = null;

		try{
			con = DBManager.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from user_data where login_id = ? and password = ?");
			stmt.setString(1, userData.getLoginId());
			stmt.setString(2, etc.pass(userData.getPassword()));
			ResultSet rs = stmt.executeQuery();

			if(rs.next()) {
				userData.setId(rs.getInt("id"));
				userData.setName(rs.getString("name"));
				userData.setAddress(rs.getString("address"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return userData;
	}

	//ユーザーIDからユーザー情報を取得
	public static UserDataBeans userData(int id) {
		Connection con = null;
		UserDataBeans userData = new UserDataBeans();
		try {
			con = DBManager.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from user_data where id = ?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				userData.setId(rs.getInt("id"));
				userData.setLoginId(rs.getString("login_id"));
				userData.setName(rs.getString("name"));
				userData.setAddress(rs.getString("address"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return userData;
	}

	//ユーザー情報を更新
	public static void userDataUpdate(UserDataBeans userData, int userId){
		Connection con = null;
		try {
			con = DBManager.getConnection();
			String sql = "update user_data set login_id = ?, name = ?, address = ?, password = ? where id = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, userData.getLoginId());
			stmt.setString(2, userData.getName());
			stmt.setString(3, userData.getAddress());
			stmt.setString(4, etc.pass(userData.getPassword()));
			stmt.setInt(5, userId);
			stmt.executeUpdate();

		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try{
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
