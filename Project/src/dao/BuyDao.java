package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import beans.BuyDataBeans;

public class BuyDao {
	//購入情報を登録
	public static int insertBuyData(BuyDataBeans buyData) {
		Connection con = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			String sql = "insert into buy (user_id, total_price, buy_date, delivery) value(?,?,?,?)";
			PreparedStatement stmt = con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, buyData.getUserId());
			stmt.setInt(2, buyData.getTotalPrice());
			stmt.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			stmt.setInt(4, buyData.getDeliveryId());
			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	return autoIncKey;
	}

	//購入IDから商品情報を取得
	public static BuyDataBeans getBuyData(int buyId) {
		Connection con = null;
		BuyDataBeans buyData = new BuyDataBeans();
		try {
			con = DBManager.getConnection();
			String sql = "select * from buy join delivery on buy.delivery = delivery.id where buy.id = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, buyId);
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				buyData.setId(rs.getInt("id"));
				buyData.setUserId(rs.getInt("user_id"));
				buyData.setTotalPrice(rs.getInt("total_price"));
				buyData.setBuyDate(rs.getTimestamp("buy_date"));
				buyData.setDeliveryId(rs.getInt("delivery"));
				buyData.setDeliveryName(rs.getString("delivery.name"));
				buyData.setDeliveryPrice(rs.getInt("delivery.price"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return buyData;
	}

	//ユーザーIDから購入履歴を取得
	public static ArrayList<BuyDataBeans> getBuyDataByUserId(int userId) {
		Connection con = null;
		ArrayList<BuyDataBeans> buyData = new ArrayList<BuyDataBeans>();
		try {
			con = DBManager.getConnection();
			String sql = "select * from buy join delivery on buy.delivery = delivery.id where buy.user_id = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, userId);
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				BuyDataBeans BD = new BuyDataBeans();
				BD.setId(rs.getInt("id"));
				BD.setUserId(rs.getInt("user_id"));
				BD.setTotalPrice(rs.getInt("total_price"));
				BD.setBuyDate(rs.getTimestamp("buy_date"));
				BD.setDeliveryId(rs.getInt("delivery"));
				BD.setDeliveryName(rs.getString("delivery.name"));
				BD.setDeliveryPrice(rs.getInt("delivery.price"));
				buyData.add(BD);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return buyData;
	}
}
