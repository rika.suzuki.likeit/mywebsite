package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.DeliveryBeans;

public class DeliveryDao {

	//配送方法を取得
	public static ArrayList<DeliveryBeans> dList(){
			Connection con = null;
			ArrayList<DeliveryBeans> dList = new ArrayList<DeliveryBeans>();
			try {
				con = DBManager.getConnection();
				PreparedStatement stmt = con.prepareCall("select * from delivery");
				ResultSet rs = stmt.executeQuery();

				while(rs.next()) {
					DeliveryBeans delivery = new DeliveryBeans();
					delivery.setId(rs.getInt("id"));
					delivery.setName(rs.getString("name"));
					dList.add(delivery);
				}

			}catch(SQLException e) {
				e.printStackTrace();
			}finally {
				if(con != null) {
					try {
						con.close();
					}catch(SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return dList;
	}

	//配送IDから詳細を取得
	public static DeliveryBeans deliveryDetail(int id) {
		Connection con = null;
		DeliveryBeans deliveryDetail = new DeliveryBeans();
		try {
			con = DBManager.getConnection();
			PreparedStatement stmt = con.prepareStatement("select * from delivery where id = ?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				deliveryDetail.setId(rs.getInt("id"));
				deliveryDetail.setName(rs.getString("name"));
				deliveryDetail.setPrice(rs.getInt("price"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return deliveryDetail;
	}
}
