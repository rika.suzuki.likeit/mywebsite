package beans;

import java.io.Serializable;

public class BuyDetailBeans implements Serializable{
	private int id;
	private int buyId;
	private int itemId;
	private String itemName;
	private String itemImage;
	private int itemPrice;
	private int itemSize;
	private String itemSizeName;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBuyId() {
		return buyId;
	}
	public void setBuyId(int buyId) {
		this.buyId = buyId;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemImage() {
		return itemImage;
	}
	public void setItemImage(String itemImage) {
		this.itemImage = itemImage;
	}
	public int getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(int itemPrice) {
		this.itemPrice = itemPrice;
	}
	public int getItemSize() {
		return itemSize;
	}
	public void setItemSize(int itemSize) {
		this.itemSize = itemSize;
	}
	public String getItemSizeName() {
		return itemSizeName;
	}
	public void setItemSizeName(String itemSizeName) {
		this.itemSizeName = itemSizeName;
	}

}
