package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.BuyDetailBeans;
import dao.BuyDetailDao;

/**
 * Servlet implementation class BuyDetail
 */
@WebServlet("/BuyDetail")
public class BuyDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//購入IDを取得
		int buyId = Integer.parseInt(request.getParameter("buyId"));

		//購入IDから購入履歴詳細を取得
		ArrayList<BuyDetailBeans> buyDetail = BuyDetailDao.getBuyDetailData(buyId);

		//購入履歴詳細画面へ遷移
		request.setAttribute("buyDetail", buyDetail);
		request.getRequestDispatcher("/WEB-INF/jsp/buydatadetail.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
