package Servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDataDao;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		//入力されたユーザー情報を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		UserDataBeans loginUser = new UserDataBeans();
		loginUser.setLoginId(loginId);
		loginUser.setPassword(password);

		//ログインしたユーザー情報を取得
		UserDataBeans userData = UserDataDao.loginCheck(loginUser);

		if(userData.getName() == null) {
			String msg = "入力内容が異なります。";
			request.setAttribute("msg",msg);
			request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
		}else {
			session.setAttribute("userData",userData);
			response.sendRedirect("Top");
		}
	}

}
