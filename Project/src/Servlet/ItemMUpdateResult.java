package Servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemMUpdateResult
 */
@WebServlet("/ItemMUpdateResult")
public class ItemMUpdateResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//入力された商品情報を取得
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String price = request.getParameter("price");
		String category = request.getParameter("category");
		String image = request.getParameter("image");
		String detail = request.getParameter("detail");

		ItemBeans itemUpdate = new ItemBeans();
		itemUpdate.setId(Integer.parseInt(id));
		itemUpdate.setName(name);
		itemUpdate.setPrice(Integer.parseInt(price));
		itemUpdate.setCategory(Integer.parseInt(category));
		itemUpdate.setImage(image);
		itemUpdate.setDetail(detail);

		//選択された商品IDの詳細を更新
		ItemDao.itemUpdate(itemUpdate);

		//更新した商品詳細を取得
		ItemBeans itemDetail = ItemDao.itemDetail(id);

		//更新完了画面へ遷移
		request.setAttribute("itemDetail", itemDetail);
		request.getRequestDispatcher("/WEB-INF/jsp/itemmupdateresult.jsp").forward(request, response);

	}

}
