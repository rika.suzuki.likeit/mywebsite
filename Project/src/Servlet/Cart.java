package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;

/**
 * Servlet implementation class Cart
 */
@WebServlet("/Cart")
public class Cart extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//カート情報を取得する。カートが存在しない場合、カート作成する。
		HttpSession session = request.getSession();
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");
		if(cart == null) {
			cart = new ArrayList<ItemBeans>();
		}

		String msg = "";
		if(cart.size() == 0) {
			msg = "カートに商品がありません。";
		}

		//カート画面へ遷移
		request.setAttribute("msg", msg);
		request.setAttribute("cart", cart);
		request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
