package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;

/**
 * Servlet implementation class ItemDelete
 */
@WebServlet("/ItemDelete")
public class ItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//削除する商品IDを取得
		String[] idList = request.getParameterValues("itemDelete");

		//カート情報を取得
		HttpSession session = request.getSession();
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

		//カートから商品を削除
		String msg = "";
		if(idList != null) {
			for(String itemId:idList) {
				for(ItemBeans itemCart:cart){
					if(Integer.parseInt(itemId) == itemCart.getId()) {
						cart.remove(itemCart);
						break;
					}
				}
			}
			msg = "商品を削除しました。";
		}else {
			msg = "商品が選択されていません。";
		}

		//カート画面へ遷移
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp").forward(request, response);

	}

}
