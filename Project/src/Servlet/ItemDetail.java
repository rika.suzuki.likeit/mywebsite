package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemBeans;
import beans.SizeBeans;
import dao.ItemDao;
import dao.SizeDao;

/**
 * Servlet implementation class ItemDetail
 */
@WebServlet("/ItemDetail")
public class ItemDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//商品IDから商品詳細を取得
		String id = request.getParameter("id");
		ItemBeans itemDetail = ItemDao.itemDetail(id);

		//サイズを取得
		ArrayList<SizeBeans> sizeList = SizeDao.sizeList();

		//商品詳細画面へ遷移
		request.setAttribute("sizeList", sizeList);
		request.setAttribute("itemDetail", itemDetail);
		request.getRequestDispatcher("/WEB-INF/jsp/itemdetail.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
