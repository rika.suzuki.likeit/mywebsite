package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CategoryBeans;
import dao.CategoryDao;

/**
 * Servlet implementation class ItemMRegist
 */
@WebServlet("/ItemMRegist")
public class ItemMRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//商品カテゴリーを取得
		ArrayList<CategoryBeans> categoryList = CategoryDao.getCategory();
		request.setAttribute("categoryList", categoryList);

		//管理画面の商品新規登録へ遷移
		request.getRequestDispatcher("/WEB-INF/jsp/itemmregist.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
