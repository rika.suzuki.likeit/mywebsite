package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.DeliveryBeans;
import beans.ItemBeans;
import beans.UserDataBeans;
import dao.DeliveryDao;

/**
 * Servlet implementation class BuyConfirm
 */
@WebServlet("/BuyConfirm")
public class BuyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//配送方法IDから情報を取得
		int id = Integer.parseInt(request.getParameter("delivery"));
		DeliveryBeans deliveryDetail = DeliveryDao.deliveryDetail(id);

		//カート情報を取得
		HttpSession session = request.getSession();
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

		//合計金額を算出
		int totalPrice = deliveryDetail.getPrice();
		for(ItemBeans cartItem : cart) {
			totalPrice += cartItem.getPrice();
		}

		//購入商品情報
		BuyDataBeans buyData = new BuyDataBeans();
		UserDataBeans userData = (UserDataBeans) session.getAttribute("userData");
		buyData.setUserId(userData.getId());
		buyData.setTotalPrice(totalPrice);
		buyData.setDeliveryId(deliveryDetail.getId());
		buyData.setDeliveryName(deliveryDetail.getName());
		buyData.setDeliveryPrice(deliveryDetail.getPrice());

		//購入確定画面へ遷移
		session.setAttribute("buyData", buyData);
		request.getRequestDispatcher("/WEB-INF/jsp/buyconfirm.jsp").forward(request, response);

	}

}
