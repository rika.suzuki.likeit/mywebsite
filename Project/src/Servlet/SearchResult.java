package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CategoryBeans;
import beans.ItemBeans;
import dao.CategoryDao;
import dao.ItemDao;

/**
 * Servlet implementation class SearchResult
 */
@WebServlet("/SearchResult")
public class SearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//カテゴリーIDを取得
		String searchWord = request.getParameter("categoryId");

		//商品リストを取得
		ArrayList<ItemBeans> itemList = ItemDao.searchItemByCategory(Integer.parseInt(searchWord));

		//カテゴリー名取得
		CategoryBeans categoryDetail = CategoryDao.categoryDetail(Integer.parseInt(searchWord));
		String categoryName = categoryDetail.getName();
		request.setAttribute("searchWord", categoryName);

		//検索結果画面へ遷移
		request.setAttribute("itemList", itemList);
		request.getRequestDispatcher("/WEB-INF/jsp/searchresult.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//検索ワードを取得
		request.setCharacterEncoding("UTF-8");
		String searchWord = request.getParameter("searchWord");
		request.setAttribute("searchWord", searchWord);

		//商品リストを取得
		ArrayList<ItemBeans> itemList = ItemDao.searchItemByWord(searchWord);

		//検索結果画面へ遷移
		request.setAttribute("itemList", itemList);
		request.getRequestDispatcher("/WEB-INF/jsp/searchresult.jsp").forward(request, response);
	}

}
