package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.BuyDetailBeans;
import beans.ItemBeans;
import dao.BuyDao;
import dao.BuyDetailDao;

/**
 * Servlet implementation class BuyResult
 */
@WebServlet("/BuyResult")
public class BuyResult extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//カート情報、購入情報を取得
		HttpSession session = request.getSession();
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");
		BuyDataBeans buyData = (BuyDataBeans) session.getAttribute("buyData");

		//DBへ購入情報を登録
		int buyId = BuyDao.insertBuyData(buyData);

		//DBへ購入詳細情報を登録
		for(ItemBeans cartItem : cart) {
			BuyDetailBeans buyDetailData = new BuyDetailBeans();
			buyDetailData.setBuyId(buyId);
			buyDetailData.setItemId(cartItem.getId());
			buyDetailData.setItemSize(cartItem.getSize());
			BuyDetailDao.insertBuyDetailData(buyDetailData);
		}

		//カート情報削除
		session.removeAttribute("cart");

		//購入IDから商品情報を取得
		BuyDataBeans resultBD = BuyDao.getBuyData(buyId);

		//購入IDから購入詳細を取得
		ArrayList<BuyDetailBeans> resultBDD = BuyDetailDao.getBuyDetailData(buyId);

		//購入完了画面へ遷移
		request.setAttribute("resultBD", resultBD);
		request.setAttribute("resultBDD", resultBDD);
		request.getRequestDispatcher("/WEB-INF/jsp/buyresult.jsp").forward(request, response);
	}

}
