package Servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemMDetail
 */
@WebServlet("/ItemMDetail")
public class ItemMDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//商品IDから商品詳細を取得
		String id = request.getParameter("id");
		ItemBeans itemDetail = ItemDao.itemDetail(id);

		//管理画面の商品詳細へ遷移
		request.setAttribute("itemDetail", itemDetail);
		request.getRequestDispatcher("/WEB-INF/jsp/itemmdetail.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
