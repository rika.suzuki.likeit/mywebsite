package Servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserDataBeans;
import dao.UserDataDao;

/**
 * Servlet implementation class UserRegistResult
 */
@WebServlet("/UserRegistResult")
public class UserRegistResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//ユーザー情報を取得
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String password = request.getParameter("password");

		UserDataBeans userData = new UserDataBeans();
		userData.setLoginId(loginId);
		userData.setName(name);
		userData.setAddress(address);
		userData.setPassword(password);

		//登録確定or修正判定
		String registCheck = request.getParameter("userregistconfirm");

		switch (registCheck) {
		case "cancel":
			request.setAttribute("userData", userData);
			request.getRequestDispatcher("/WEB-INF/jsp/userregist.jsp").forward(request, response);
			break;

		case "regist":
			UserDataDao.userRegist(userData);
			request.setAttribute("userData", userData);
			request.getRequestDispatcher("/WEB-INF/jsp/userregistresult.jsp").forward(request, response);
			break;
		}
	}
}
