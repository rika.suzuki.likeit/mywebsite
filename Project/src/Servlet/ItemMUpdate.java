package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CategoryBeans;
import beans.ItemBeans;
import dao.CategoryDao;
import dao.ItemDao;

/**
 * Servlet implementation class ItemMUpdate
 */
@WebServlet("/ItemMUpdate")
public class ItemMUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//更新する商品IDを取得
		String id = request.getParameter("updateid");
		ItemBeans itemDetail = ItemDao.itemDetail(id);

		//商品カテゴリーを取得
		ArrayList<CategoryBeans> categoryList = CategoryDao.getCategory();

		//更新画面へ遷移
		request.setAttribute("categoryList", categoryList);
		request.setAttribute("itemDetail", itemDetail);
		request.getRequestDispatcher("/WEB-INF/jsp/itemmupdate.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
