package Servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDataDao;

/**
 * Servlet implementation class UserDataUpdateResult
 */
@WebServlet("/UserDataUpdateResult")
public class UserDataUpdateResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッションのユーザー情報を取得
		HttpSession session = request.getSession();
		UserDataBeans userData =  (UserDataBeans) session.getAttribute("userData");
		int userId = userData.getId();
		String oldLoginId = userData.getLoginId();

		//入力されたユーザー情報を取得
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");

		UserDataBeans userDataUpdate = new UserDataBeans();
		userDataUpdate.setLoginId(loginId);
		userDataUpdate.setName(name);
		userDataUpdate.setAddress(address);
		userDataUpdate.setPassword(password1);

		//入力内容の確認
		String message = "";

		//パスワードが不一致の場合
		if (!password1.equals(password2)) {
			message += "パスワードが一致しません";
		}

		//ログインIDが使用済の場合
		if(!(loginId.equals(oldLoginId))) {
			if (UserDataDao.loginIdCheck(loginId)) {
				message += "使用済のログインIDです";
			}
		}else {
			System.out.println("登録可");
		}

		if(message.length() == 0) {
			//更新可：ユーザー情報を更新
			UserDataDao.userDataUpdate(userDataUpdate, userId);

			//ユーザー情報を新たに取得
			UserDataBeans newUserData = UserDataDao.userData(userId);
			session.setAttribute("userData", newUserData);

			//更新完了画面へ遷移
			request.getRequestDispatcher("/WEB-INF/jsp/userdataupdateresult.jsp").forward(request, response);
		}else {
			//更新不可：更新画面へ遷移
			request.setAttribute("userData", userData);
			request.setAttribute("message", message);
			request.getRequestDispatcher("/WEB-INF/jsp/userdataupdate.jsp").forward(request, response);
		}
	}

}
