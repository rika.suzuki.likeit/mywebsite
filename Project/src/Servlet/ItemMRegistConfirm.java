package Servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemMRegistConfirm
 */
@WebServlet("/ItemMRegistConfirm")
public class ItemMRegistConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//入力された商品情報を取得
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");
		String price = request.getParameter("price");
		String category = request.getParameter("category");
		String image = request.getParameter("image");
		String detail = request.getParameter("detail");

		ItemBeans itemRegist = new ItemBeans();
		itemRegist.setName(name);
		itemRegist.setPrice(Integer.parseInt(price));
		itemRegist.setCategory(Integer.parseInt(category));
		itemRegist.setImage(image);
		itemRegist.setDetail(detail);

		//DBへ商品を登録
		String id = String.valueOf(ItemDao.itemRegist(itemRegist));

		//登録した商品詳細を取得
		ItemBeans itemDetail = ItemDao.itemDetail(id);

		//登録終了画面へ遷移
		request.setAttribute("itemDetail", itemDetail);
		request.getRequestDispatcher("/WEB-INF/jsp/itemmregistresult.jsp").forward(request, response);

	}

}
