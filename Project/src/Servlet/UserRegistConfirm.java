package Servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserDataBeans;
import dao.UserDataDao;

/**
 * 新規ユーザー登録確認画面
 */
@WebServlet("/UserRegistConfirm")
public class UserRegistConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//入力されたユーザー情報を取得
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");

		UserDataBeans userData = new UserDataBeans();
		userData.setLoginId(loginId);
		userData.setName(name);
		userData.setAddress(address);
		userData.setPassword(password1);


		//入力内容の確認
		String message = "";

		//パスワードが不一致の場合
		if (!password1.equals(password2)) {
			message += "パスワードが一致しません";
		}

		//ログインIDが使用済の場合
		if (UserDataDao.loginIdCheck(loginId)) {
			message += "使用済のログインIDです";
		}


		if(message.length() == 0) {
			//登録可：登録内容確認画面へ遷移
			request.setAttribute("userData", userData);
			request.getRequestDispatcher("/WEB-INF/jsp/userregistconfirm.jsp").forward(request, response);
		}else {
			//登録不可：登録画面へ遷移
			request.setAttribute("userData", userData);
			request.setAttribute("message", message);
			request.getRequestDispatcher("/WEB-INF/jsp/userregist.jsp").forward(request, response);
		}
	}

}
