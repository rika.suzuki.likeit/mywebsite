package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemMList
 */
@WebServlet("/ItemMList")
public class ItemMList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//全商品リストを取得
		ArrayList<ItemBeans> itemList = ItemDao.AllItem();

		//商品管理画面へ遷移
		request.setAttribute("itemList", itemList);
		request.getRequestDispatcher("/WEB-INF/jsp/itemmlist.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
