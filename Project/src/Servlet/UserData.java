package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.UserDataBeans;
import dao.BuyDao;

/**
 * Servlet implementation class UserData
 */
@WebServlet("/UserData")
public class UserData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ユーザーIDを取得
		HttpSession session = request.getSession();
		UserDataBeans userData = (UserDataBeans) session.getAttribute("userData");
		int userId = userData.getId();

		//ユーザーIDから購入履歴を取得
		ArrayList<BuyDataBeans> buyData = BuyDao.getBuyDataByUserId(userId);
		if(buyData.isEmpty()) {
			request.setAttribute("msg", "購入履歴はありません");
		}else {
			request.setAttribute("buyData", buyData);
		}

		//ユーザー情報画面へ遷移
		request.getRequestDispatcher("/WEB-INF/jsp/userdata.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
