package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryBeans;
import beans.ItemBeans;
import dao.CategoryDao;
import dao.ItemDao;

/**
 * Top画面
 */
@WebServlet("/Top")
public class Top extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		//商品カテゴリーを取得
		ArrayList<CategoryBeans> categoryList = CategoryDao.getCategory();

		//ランダムにピックアップ商品を選択
		ArrayList<ItemBeans> pickupItemList = ItemDao.pickupItem(3);

		//TOP画面へ遷移
		session.setAttribute("categoryList", categoryList);
		request.setAttribute("pickupItemList", pickupItemList);
		request.getRequestDispatcher("/WEB-INF/jsp/top.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
