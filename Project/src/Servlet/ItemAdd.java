package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.SizeBeans;
import dao.ItemDao;
import dao.SizeDao;

/**
 * Servlet implementation class ItemAdd
 */
@WebServlet("/ItemAdd")
public class ItemAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//商品ID・サイズを取得
		String itemId = request.getParameter("id");
		String sizeId = request.getParameter("size");

		//サイズIDから詳細を取得
		SizeBeans itemSize = SizeDao.sizeDetail(Integer.parseInt(sizeId));

		//商品情報を取得
		ItemBeans itemDetail = ItemDao.itemDetail(itemId);
		itemDetail.setSize(Integer.parseInt(sizeId));
		itemDetail.setSizeName(itemSize.getName());

		//カート情報を取得する。カートが存在しない場合、カートを作成する。
		HttpSession session = request.getSession();
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");
		if (cart == null) {
				cart = new ArrayList<ItemBeans>();
		}

		//カートに追加
		cart.add(itemDetail);

		//カート画面へ遷移
		session.setAttribute("cart", cart);
		request.setAttribute("msg", "カートに商品を追加しました。");
		request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp").forward(request, response);
	}

}
