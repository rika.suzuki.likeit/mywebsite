package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.DeliveryBeans;
import beans.ItemBeans;
import beans.UserDataBeans;
import dao.DeliveryDao;

/**
 * Servlet implementation class Buy
 */
@WebServlet("/Buy")
public class Buy extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//カート情報を取得
		HttpSession session = request.getSession();
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

		//ログイン情報を取得：未ログインの場合、ログイン画面へ遷移
		UserDataBeans loginUser = (UserDataBeans) session.getAttribute("userData");
		if(loginUser == null) {
			response.sendRedirect("Login");
		}

		//カートに商品がない場合、カート画面へ遷移
		else if(cart.size() == 0){
			String msg = "カートに商品がありません。";
			request.setAttribute("msg", msg);
			request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp").forward(request, response);
		}else {

		//DBから配送方法を取得
		ArrayList<DeliveryBeans> deliveryList = DeliveryDao.dList();

		//購入画面へ遷移
		session.setAttribute("cart", cart);
		request.setAttribute("deliveryList", deliveryList);
		request.getRequestDispatcher("/WEB-INF/jsp/buy.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
