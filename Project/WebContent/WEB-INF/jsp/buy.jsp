<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>購入</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/header/header.jsp" />

    <div class="container">
        <br>
        <br>
        <br>

        <h2 class="text-center">購入手続き</h2>

        <br>
        <br>

        <form action="BuyConfirm" method="post">

            <br>
            <br>

            <table class="table table-striped">
                <thead style="background-color:#e1bee7">
                    <tr>
                        <th class="text-center">商品名</th>
                        <th class="text-center">サイズ</th>
                        <th class="text-center">価格</th>
                    </tr>
                </thead>
                <tbody>
                	<c:forEach var="item" items="${cart}">
	                    <tr>
	                        <td class="text-center">${item.name}</td>
	                        <td class="text-center">${item.sizeName}</td>
	                        <td class="text-center">${item.price}　円</td>
	                    </tr>
					</c:forEach>
                </tbody>
            </table>

            <br>

             <div class="text-center">
                <label for="delivery">配送方法</label>
                <select name="delivery" id="delivery">
                	<c:forEach var="dList" items="${deliveryList}">
                    	<option value="${dList.id}">${dList.name}</option>
                	</c:forEach>
                </select>
            </div>

            <br>
            <br>

            <div class="col-sm-6 offset-sm-3">
                <button type="submit" class="btn btn-block" style="background-color:#e1bee7">購入確認</button>
            </div>
        </form>

    </div>
</body>
</html>
