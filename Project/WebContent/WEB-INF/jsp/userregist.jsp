<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>新規登録</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<jsp:include page="/header/header.jsp" />

    <div class="container">

        <br>
        <br>
        <br>

        <h2 class="text-center">新規登録</h2>

		<div class="col-sm-8 offset-sm-2">
        	<br>

	        <c:if test="${message != null}">
	        	<div class="alert alert-danger role=alert">
	        		${message}
	        	</div>
	        </c:if>

	        <br>

            <form action="UserRegistConfirm" method="post">
                <div class="form-group row">
                    <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="loginId" id="loginId" value="${userData.loginId}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">名前</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" id="name" value="${userData.name}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-2 col-form-label">住所</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="address" id="address" value="${userData.address}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password2" class="col-sm-2 col-form-label">パスワード</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password1" id="password1" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password2" class="col-sm-2 col-form-label">パスワード(確認)</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password2" id="password2" required>
                    </div>
                </div>

                <br>

                <div class="text-center col-sm-6 offset-sm-3">
                    <button type="submit" class="btn btn-block" style="background-color:#e1bee7">確認</button>
                </div>
            </from>
        </div>
    </div>
</body>
</html>