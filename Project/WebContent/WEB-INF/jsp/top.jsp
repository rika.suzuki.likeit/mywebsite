<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>TOPページ</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/header/header.jsp" />

	<div class="container-fluid">

		<br><br>
		<h1 class="text-center">通販サイト</h1>
		<br><br>

        <div class="col-sm-6 offset-sm-3">
            <form action="SearchResult" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" name ="searchWord" placeholder="キーワードを入力">
                    <span class="input-group-btn">
						<button type="submit" class="btn" style="background-color: #e1bee7">検索</button>
					</span>
				</div>
            </form>
        </div>

        <br>
        <br>

		<div class="row">
			<div class="col-sm-2">
                <ul class="list-group list-group-flush" >
                	<c:forEach var="cList" items="${categoryList}">
                    	<li class="list-group-item"><a href="SearchResult?categoryId=${cList.id}">${cList.name}</a></li>
                	</c:forEach>
                </ul>
			</div>

            <div class="col-sm-8 offset-sm-1">

                <h2>ピックアップ</h2>

                <br>

                <div class="card-deck row">
	               	<c:forEach var="item" items="${pickupItemList}">
	                   <div class="card col-sm-4">
	                       <div class="card-image">
	                           <a href="ItemDetail?id=${item.id}"><img src="img/${item.image}"></a>
	                       </div>
	                       <div class="card-body">
	                           <h4 class="card-title">${item.name}</h4>
	                           <div>${item.price}円</div>
	                       </div>
	                   </div>
	                </c:forEach>
                </div>
            </div>
        </div>
	</div>
</body>
</html>