<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>管理画面／商品詳細</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<jsp:include page="/header/header.jsp" />

    <div class="container">

        <br>
        <br>

        <a href="ItemMList">商品一覧へ戻る</a>

        <h2 class="text-center">商品詳細</h2>

        <br>
        <br>

        <div class="row">
            <div class="text-center col-sm-3 offset-sm-1">
                <a href="ItemMUpdate?updateid=${itemDetail.id}"><button type="button" class="btn btn-block">更新</button></a>
            </div>
            <div class="text-center col-sm-3 offset-sm-4">
                <a href="ItemMDelete?deleteid=${itemDetail.id}"><button type="button" class="btn btn-block">削除</button></a>
            </div>
        </div>

        <br>
        <br>

        <div class="row">
           <div class="card-image">
               <img src="img/${itemDetail.image}">
           </div>
       </div>

        <br>
        <br>

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">商品名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control-plaintext" name="name" id="name" value="${itemDetail.name}" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label for="price" class="col-sm-2 col-form-label">価格</label>
            <div class="col-sm-10">
                <input type="text" class="form-control-plaintext" name="price" id="price" value="${itemDetail.price}円" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label for="category" class="col-sm-2 col-form-label">カテゴリー</label>
            <div class="col-sm-10">
                <input type="text" class="form-control-plaintext" name="category" id="category" value="${itemDetail.categoryName}" readonly>
            </div>
        </div>

        <div class="row">
            <label for="detail" class="col-sm-2 col-form-label">商品説明</label>
            	<div class="col-sm-10">${itemDetail.detail}</div>
        </div>
    </div>

</body>
</html>