<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>ログイン</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/header/header.jsp" />

    <div class="container">

		<div class="row">
            <div class="col-sm-8 offset-sm-2">

            	<br>
		        <br>
		        <br>

		        <h2 class="text-center">ログイン</h2>

		        <!--エラーメッセージがある場合-->
		        <c:if test="${msg != null}">
		        	<br>
		            <div class="alert alert-danger" role="alert">
		                ${msg}
		            </div>
		        </c:if>

		        <br>

                <form action="Login" method="post">
                    <div class="form-group row">
                        <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
                        <div class="col-sm-10">
                            <input type="text" name="loginId" class="form-control" id="loginId">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">パスワード</label>
                        <div class="col-sm-10">
                            <input type="password" name="password" class="form-control" id="password">
                        </div>
                    </div>
                    <br>
                    <div class="text-center col-sm-6 offset-sm-3">
                        <button type="submit" class="btn btn-block" style="background-color:#e1bee7">ログイン</button>
                    </div>

                    <br>
                    <br>

                    <div class="text-right">
                        <a href="Regist">新規登録</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>
</html>