<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>新規登録</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<jsp:include page="/header/header.jsp" />

    <div class="container">

        <br>
        <br>
        <br>

        <h2 class="text-center">入力内容確認</h2>

        <br>

        <div class="col-sm-8 offset-sm-2">
        	<form action="UserRegistResult" method="post">
	            <div class="form-group row">
	                <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
	                <div class="col-sm-10">
	                    <input type="text" class="form-control-plaintext" name="loginId" id="loginId" value="${userData.loginId}" readonly>
	                </div>
	            </div>
	            <div class="form-group row">
	                <label for="name" class="col-sm-2 col-form-label">名前</label>
	                <div class="col-sm-10">
	                    <input type="text" class="form-control-plaintext" name="name" id="name" value="${userData.name}">
	                </div>
	            </div>
	            <div class="form-group row">
	                <label for="address" class="col-sm-2 col-form-label">住所</label>
	                <div class="col-sm-10">
	                    <input type="text" class="form-control-plaintext" name="address" id="address" value="${userData.address}">
	                </div>
	            </div>
	            <div class="form-group row">
	                <label for="password" class="col-sm-2 col-form-label">パスワード</label>
	                <div class="col-sm-10">
	                    <input type="password" class="form-control-plaintext" name="password" id="password" value="${userData.password}">
	                </div>
	            </div>

	            <br>

	            <p class="text-center">上記の内容で登録します。</p>

	            <br>

	            <div class="row">
	                <div class="col-sm-3 offset-sm-1">
	                    <button type="submit" class="btn btn-block" style="background-color:#e1bee7" name="userregistconfirm" value="cancel">修正</button>
	                </div>
	                <div class="col-sm-3 offset-sm-4">
	                    <button type="submit" class="btn btn-block" style="background-color:#e1bee7" name="userregistconfirm" value="regist">登録</button>
	                </div>
	            </div>
			</form>
		</div>
    </div>
</body>
</html>