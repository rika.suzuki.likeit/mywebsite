<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
<title>商品詳細</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<jsp:include page="/header/header.jsp" />

    <br>

    <div class="container">

<!--        <br>
		<a href="Top">一覧へ戻る</a>
        <br>
        <br> -->

        <h2 class="text-center">商品詳細</h2>

        <br>
        <br>
        <br>
        <br>

        <div class="row">
            <div class="col-sm-6 ">
                <div class="card-image">
                    <img src="img/${itemDetail.image}">
                </div>
            </div>

            <div class="col-sm-5 offset-sm-1">
                <h3>${itemDetail.name}</h3>
                <br>
                <h5>${itemDetail.price}　円</h5>
                <br>
                <p>${itemDetail.detail}</p>
                <br>

                <form action="ItemAdd?id=${itemDetail.id}" method="post">

                    <div>
                        サイズ：
                        <select name="size">
							<c:forEach var="sList" items="${sizeList}">
                            	<option value="${sList.id}">${sList.name}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <br>
                    <br>

                    <div class="row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-block" style="background-color:#e1bee7">カートに追加</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>
</html>
