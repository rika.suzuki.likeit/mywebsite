<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>カート</title>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
crossorigin="anonymous">
</head>
<body>

	<jsp:include page="/header/header.jsp" />

    <div class="container">
        <br>
        <br>
        <br>

        <h2 class="text-center">カート</h2>

		<br>
		<p>${msg}</p>
        <br>
        <br>

        <form action="ItemDelete" method="post">

            <div class="row">
                <div class="col-sm-4 offset-sm-1">
                    <button type="submit" class="btn btn-block" style="background-color:#e1bee7">選択した商品を削除</button>
                </div>
                <div class="col-sm-4 offset-sm-2">
                    <a href="Buy"><button type="button" class="btn btn-block" style="background-color:#e1bee7">購入画面へ進む</button></a>
                </div>
            </div>

            <br>
            <br>

            <div class="card-deck row">
            	<c:forEach var="item" items="${cart}">
	                <div class="card col-sm-4">
	                    <div class="card-image">
	                        <img src="img/${item.image}">
	                    </div>
	                    <div class="card-body">
	                        <h3 class="card-title">${item.name}</h3>
	                        <p>${item.price} 円</p>
	                        <p>サイズ：${item.sizeName}</p>
	                        <div class="form-ckeck">
	                            <input type="checkbox" name="itemDelete" value="${item.id}">
	                            <label class="form-check-label">削除</label>
	                        </div>
	                    </div>
	                </div>
				</c:forEach>
            </div>
        </form>

    </div>
</body>
</html>
