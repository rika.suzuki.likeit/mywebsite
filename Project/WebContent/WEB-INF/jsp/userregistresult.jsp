<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>登録完了</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<jsp:include page="/header/header.jsp" />

    <div class="container">

        <br>
        <br>
        <br>

        <h2 class="text-center">ユーザー登録完了</h2>

        <br>

        <div class="col-sm-8 offset-sm-2">
            <div class="form-group row">
                <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control-plaintext" id="loginId" value="${userData.loginId}">
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">名前</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control-plaintext" id="name" value="${userData.name}">
                </div>
            </div>
            <div class="form-group row">
                <label for="address" class="col-sm-2 col-form-label">住所</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control-plaintext" id="address" value="${userData.address}">
                </div>
            </div>

            <br>

            <p class="text-center">上記の内容で登録しました。</p>

            <br>

	            <div class="col-sm-6 offset-sm-3">
	                <a href="Login">
	                	<button type="button" class="btn btn-block" style="background-color:#e1bee7">ログイン画面へ</button>
	                </a>
	            </div>
        </div>
    </div>
</body>
</html>