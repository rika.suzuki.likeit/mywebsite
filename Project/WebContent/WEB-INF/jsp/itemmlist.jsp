<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>管理画面／商品一覧</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/header/header.jsp" />

    <div class="container">

        <br>
        <br>

        <h2 class="text-center">商品一覧</h2>

        <br>
        <br>

        <div class="col-sm-6 offset-sm-3">
            <a href="ItemMRegist"><button type="button" class="btn btn-block">商品新規登録</button></a>
        </div>

        <br>
        <br>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Number</th>
                    <th>商品名</th>
                    <th>価格</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            	<c:forEach var="item" items="${itemList}">
		            <tr>
		                <td>${item.id}</td>
		                <td>${item.name}</td>
		                <td>${item.price}円</td>
		                <td><a href="ItemMDetail?id=${item.id}">詳細</a></td>
		            </tr>
				</c:forEach>
            </tbody>

        </table>
    </div>
</body>
</html>