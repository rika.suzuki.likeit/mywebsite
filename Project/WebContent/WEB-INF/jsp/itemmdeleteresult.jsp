<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>管理画面／商品削除</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<jsp:include page="/header/header.jsp" />

    <div class="container">

        <br>
        <br>

        <h2 class="text-center">商品削除完了</h2>

        <br>
        <br>

        <p>以下の商品を削除しました。</p>

        <br>
        <br>


        <div class="text-center col-sm-6 offset-sm-3">
            <a href="ItemMList"><button type="button" class="btn btn-block">商品一覧画面へ</button></a>
        </div>

        <br>
        <br>

		<div class="row">
           <div class="card-image">
               <img src="img/${itemDetail.image}">
           </div>
		</div>

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">商品名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control-plaintext" id="name" value="${itemDetail.name}">
            </div>
        </div>

        <div class="form-group row">
            <label for="price" class="col-sm-2 col-form-label">価格</label>
            <div class="col-sm-10">
                <input type="text" class="form-control-plaintext" id="price" value="${itemDetail.price}">
            </div>
        </div>

        <div class="form-group row">
            <label for="category" class="col-sm-2 col-form-label">カテゴリー</label>
            <div class="col-sm-10">
                <input type="text" class="form-control-plaintext" id="category" value="${itemDetail.categoryName}">
            </div>
        </div>

        <div class="form-group row">
            <label for="detail" class="col-sm-2 col-form-label">商品説明</label>
            <div class="col-sm-10">
                <input type="text" class="form-control-plaintext" id="detail" value="${itemDetail.detail}">
            </div>
        </div>
    </div>

</body>
</html>