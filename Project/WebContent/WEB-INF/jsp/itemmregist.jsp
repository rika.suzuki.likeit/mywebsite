<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>管理画面／商品登録</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<jsp:include page="/header/header.jsp" />

    <div class="container">

        <br>
        <br>
        <a href="ItemMList">商品一覧へ戻る</a>

        <h2 class="text-center">商品登録</h2>

        <br>
        <br>

        <form action="ItemMRegistConfirm" method="post">
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">商品名</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" id="name" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="price" class="col-sm-2 col-form-label">価格</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="price" id="price" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="image" class="col-sm-2 col-form-label">画像</label>
                <div class="col-sm-10">
                	<input type="file" src="button.gif" name="image" id="image" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="category" class="col-sm-2 col-form-label">カテゴリー</label>
                <div class="col-sm-10">
                    <select name="category" id="category">
                    	<c:forEach var="cList" items="${categoryList}">
                        	<option value="${cList.id}">${cList.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="detail" class="col-sm-2 col-form-label">商品説明</label>
                <div class="col-sm-10">
                    <textarea rows="5" class="form-control" name="detail" id="detail" required></textarea>
                </div>
            </div>

            <br>
            <br>

            <div class="text-center col-sm-6 offset-sm-3">
                <button type="submit" class="btn btn-block">登録</button>
            </div>
        </form>
    </div>

</body>
</html>