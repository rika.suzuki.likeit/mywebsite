<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>ユーザー情報</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/header/header.jsp" />

    <div class="container">
        <br>
        <br>
        <br>

        <h2 class="text-center">ユーザー情報</h2>

        <br>
        <div class="form-group row">
            <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
            <div class="col-sm-10">
                <input type="text" class="form-control-plaintext" name="loginId" id="loginId" value="${userData.loginId}" readonly>
            </div>
        </div>
		<div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">名前</label>
            <div class="col-sm-10">
                <input type="text" class="form-control-plaintext" name="name" id="name" value="${userData.name}" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label for="address" class="col-sm-2 col-form-label">住所</label>
            <div class="col-sm-10">
                <input type="text" class="form-control-plaintext" name="address" id="address" value="${userData.address}" readonly>
            </div>
        </div>
        <br>
        <div class="col-sm-6 offset-sm-3">
            <a href="UserDataUpdate"><button type="submit" class="btn btn-block" style="background-color:#e1bee7">更新</button></a>
        </div>
    </div>

    <br>
    <br>

    <div class="container">
        <h2 class="text-center">購入履歴</h2>
        <br>
		<c:if test="${msg != null}">
			<p class="text-center">${msg}</p>
		</c:if>
		<c:if test="${msg == null}">
			<table class="table table-striped table-hover">
				<thead style="background-color: #e1bee7">
					<tr>
						<th></th>
						<th>購入日時</th>
						<th>合計金額</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${buyData}">
						<tr>
							<td class="text-center"></td>
							<td>${item.formatDate}</td>
							<td>${item.totalPrice}円</td>
							<td><a href="BuyDetail?buyId=${item.id}">詳細</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>

	</div>
</body>
</html>