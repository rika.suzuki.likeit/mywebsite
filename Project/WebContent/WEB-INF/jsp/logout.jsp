<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>ログアウト</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/header/header.jsp" />

    <div class="container">

		<div class="row">
            <div class="col-sm-8 offset-sm-2">

            	<br>
		        <br>
		        <br>

		        <h3 class="text-center">ログアウトしました</h3>

				<br>
				<br>

		        <a href="Top"><button type="button" class="col-sm-6 offset-sm-3 btn btn-block" style="background-color:#e1bee7">TOPへ</button></a>

            </div>
        </div>
    </div>

</body>
</html>