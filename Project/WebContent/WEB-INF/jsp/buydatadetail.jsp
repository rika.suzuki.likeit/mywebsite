<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>購入履歴詳細</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/header/header.jsp" />

    <div class="container">
        <br>
        <a href="UserData">ユーザー情報へ戻る</a>
        <br>
        <br>

        <h2 class="text-center">購入履歴詳細</h2>

        <br>
        <br>

        <ul class="list-group list-group-flush">
			<c:forEach var="item" items="${buyDetail}">
				<li class="list-group-item">
					<div class="row">
						<div class="col-sm-6">
							<div class="acrd-image">
								<img src="img/${item.itemImage}">
							</div>
						</div>
						<div class="col-sm-5 offset-sm-1">
							<p>${item.itemName}</p>
							<br>
							<p>${item.itemPrice}円</p>
							<br>
							<p>サイズ：${item.itemSizeName}</p>
						</div>
					</div>
				</li>
			</c:forEach>
		</ul>

    </div>
</body>
</html>
