<%@ page import="beans.UserDataBeans"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<nav class="navbar navbar-expand navbar-light" style="background-color:#e1bee7">
	<div class="container-fluid">
		<a class="navbar-brand" href="Top">通販サイト</a>
		<div class="collapse navbar-collapse row" id="navbar">
			<ul class="navbar-nav ml-auto">

				<li class="nav-item">
					<c:if test="${userData.id != null}">
						<a class="nav-link" href="UserData">${userData.name}さん</a>
					</c:if>
					<c:if test="${userData.id == null}">
						<a class="nav-link" href="Login">ログイン</a>
					</c:if>
				</li>

				<li class="nav-item">
					<a class="nav-link"href="Cart">カート</a>
				</li>

				<li class="nav-item">
					<c:if test="${userData.id != null}">
						<a class="nav-link" href="Logout">ログアウト</a>
					</c:if>
					<c:if test="${userData.id == null}">
						<a class="nav-link" href="UserRegist">新規登録</a>
					</c:if>

				</li>

				<!-- 管理者のみ表示-->
				<c:if test="${userData.id == 1}">
					<li class="nav-item">
						<a class="nav-link" href="ItemMList">商品登録</a>
					</li>
				</c:if>

			</ul>
		</div>
	</div>
</nav>